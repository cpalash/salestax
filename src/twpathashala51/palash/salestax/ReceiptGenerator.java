package twpathashala51.palash.salestax;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceiptGenerator {
  private static final String INPUT_FILENAME = "res/twpathashala51/palash/salestax/input.txt";
  private static final String OUTPUT_FILENAME = "res/twpathashala51/palash/salestax/output.txt";

  private static final Map<String, TaxRateCategory> PRODUCT_LIST = new HashMap<>();

  private ReceiptGenerator() {
    populateProductList();
  }

  private void populateProductList() {
    PRODUCT_LIST.put("Book", TaxRateCategory.EXEMPTED);
    PRODUCT_LIST.put("Music CD", TaxRateCategory.BASIC);
    PRODUCT_LIST.put("Chocolate Bar", TaxRateCategory.EXEMPTED);
    PRODUCT_LIST.put("Chocolates", TaxRateCategory.EXEMPTED);
    PRODUCT_LIST.put("Perfume", TaxRateCategory.BASIC);
    PRODUCT_LIST.put("Headache Pills", TaxRateCategory.EXEMPTED);
  }

  private List<Product> readInput(Reader readerInterface) {
    List<Product> productsToBeBought = new ArrayList<>();
    try {
      BufferedReader inputBuffer = new BufferedReader(readerInterface);
      String line = null;
      while (((line = inputBuffer.readLine()) != null) && (!line.equals(""))) {
        try {
          productsToBeBought.add(parseLine(line));
        }
        catch(InvalidProductException e){
          throw new ParsingFailedException("There was an error in trying to parse the input.");
        }
      }
      inputBuffer.close();
    } catch (Exception e) {
      System.out.println("Failed to generate receipt. Exiting");
      System.exit(1);
    }
    return productsToBeBought;
  }

  private Product parseLine(String line) throws ProductNotFoundException, InvalidProductException {
    String productName = getProductName(line);
    String[] words = line.split("\\s");
    boolean imported = words[1].equals("imported");
    TaxRateCategory category = PRODUCT_LIST.get(productName);
    if(imported)
      productName = "Imported "+productName;
    try {
      Money unitPrice = Money.money(Double.parseDouble(words[words.length - 1]));
      return new Product(productName, unitPrice, imported, category);
    }
    catch(Exception e){
      throw new InvalidProductException("There was an error processing the product : "+productName);
    }
  }

  private String getProductName(String line) throws ProductNotFoundException {
    for (String productName : PRODUCT_LIST.keySet()) {
      if (line.toLowerCase().contains(productName.toLowerCase()))
        return productName;
    }
    throw new ProductNotFoundException("The specified product was not found in our list.");
  }

  private void printOutput(Writer outputInterface, Receipt receipt) {
    try {
      BufferedWriter outputWriter = new BufferedWriter(outputInterface);
      outputWriter.write(receipt.toString());
      outputWriter.newLine();
      outputWriter.close();
    } catch (IOException e) {
      System.out.println("There was an error in trying to print the output.");
    }
  }

  public static void main(String args[]) {
    ReceiptGenerator receiptGenerator = new ReceiptGenerator();
    Reader readerInterface = new InputStreamReader(System.in);
    List<Product> productsToBeBought = receiptGenerator.readInput(readerInterface);
    System.out.println(productsToBeBought.size());
    Receipt receipt = new Receipt(productsToBeBought.toArray(new Product[productsToBeBought.size()]));
    Writer outputInterface = new OutputStreamWriter(System.out);
    receiptGenerator.printOutput(outputInterface, receipt);

  }
}
