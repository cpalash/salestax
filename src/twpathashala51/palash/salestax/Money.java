package twpathashala51.palash.salestax;

import java.math.BigDecimal;

// Models currency
class Money {
  private final BigDecimal value;

  private Money(double value) {
    this.value = new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  private Money(BigDecimal value) {
    this.value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  static Money money(double value) throws InvalidMoneyException {
    if (value >= 0)
      return new Money(value);
    throw new InvalidMoneyException("Unable to money money with the specified value");
  }

  Money add(Money money) {
    return new Money(value.add(money.value));
  }

  Money multiply(double multiplier) {
    return new Money(value.multiply(new BigDecimal(multiplier)));
  }

  static Money ZERO = new Money(0);

  @Override
  public String toString() {
    return "Money{" +
        "value=" + value +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Money money = (Money) o;

    return value != null ? value.equals(money.value) : money.value == null;

  }

  @Override
  public int hashCode() {
    return value != null ? value.hashCode() : 0;
  }
}
