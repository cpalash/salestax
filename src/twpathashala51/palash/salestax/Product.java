package twpathashala51.palash.salestax;

enum TaxRateCategory {
  BASIC(0.1f), EXEMPTED(0f);
  private final double taxRate;

  TaxRateCategory(double taxRate) {
    this.taxRate = taxRate;
  }

  public double getTaxRate() {
    return this.taxRate;
  }
}

// Models an object that can be sold for a price
class Product {
  private final String productName;
  private final Money unitPrice;
  private final TaxRateCategory category;
  private final boolean imported;
  private static final double IMPORT_TAX_RATE = 0.05;

  Product(String productName, Money unitPrice, boolean imported, TaxRateCategory category) {
    this.productName = productName;
    this.unitPrice = unitPrice;
    this.category = category;
    this.imported = imported;
  }

  boolean isImported() {
    return imported;
  }

  boolean isExempted() {
    return category == TaxRateCategory.EXEMPTED;
  }

  boolean isBasic() {
    return category == TaxRateCategory.BASIC;
  }

  Money calculateBasicTax() { return unitPrice.multiply((category.getTaxRate()));}

  Money calculateImportedTax() {
    if (isImported())
      return unitPrice.multiply(IMPORT_TAX_RATE);
    return Money.ZERO;
  }

  Money finalAmount() {
    Money basicSalesTax = calculateBasicTax();
    Money importedSalesTax = calculateImportedTax();
    return (unitPrice.add(basicSalesTax)).add(importedSalesTax);
  }

  @Override
  public String toString() {
    return "Product Name ='" + productName + '\'' +
        ", Unit Price =" + unitPrice;
  }
}
