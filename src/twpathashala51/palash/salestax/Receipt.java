package twpathashala51.palash.salestax;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Models a bill listing all the products purchased along with the respective price
class Receipt {
  private final List<Product> listOfProducts = new ArrayList<>();

  Receipt(Product... listOfProducts) {
    this.listOfProducts.addAll(Arrays.asList(listOfProducts));
  }

  private Money getFinalAmountForAProduct(Product product) {
    return product.finalAmount();
  }

  private Money getSalesTaxForAProduct(Product product) {
    return product.calculateBasicTax().add(product.calculateImportedTax());
  }

  Money finalAmount() {
    Money totalAmount = Money.ZERO;
    for (Product product : listOfProducts) {
      totalAmount = totalAmount.add(getFinalAmountForAProduct(product));
    }
    return totalAmount;
  }

  Money salesTax() {
    Money totalSalesTax = Money.ZERO;
    for (Product product : listOfProducts) {
      totalSalesTax = totalSalesTax.add(getSalesTaxForAProduct(product));
    }
    return totalSalesTax;
  }

  @Override
  public String toString() {
    return "Receipt{" +
        "listOfProducts=" + listOfProducts +
        ", totalSalesTax=" + salesTax() +
        ", totalBillAmount=" + finalAmount() +
        '}';
  }
}
