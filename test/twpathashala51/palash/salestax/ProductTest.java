package twpathashala51.palash.salestax;

import org.junit.Assert;
import org.junit.Test;

public class ProductTest {
  @Test
  public void shouldReturnTrueIfProductIsImported() throws InvalidMoneyException {
    Money unitPrice = Money.money(15.00);
    Product importedProduct = new Product("Imported Book", unitPrice, true, TaxRateCategory.BASIC);
    Assert.assertTrue(importedProduct.isImported());
  }

  @Test
  public void shouldReturnTrueIfProductIsExemptedFromBasicSalesTax() throws InvalidMoneyException {
    Money unitPrice = Money.money(10.00);
    Product exemptedProduct = new Product("Exempted Food", unitPrice, true, TaxRateCategory.EXEMPTED);
    Assert.assertTrue(exemptedProduct.isExempted());
  }

  @Test
  public void shouldReturnTrueIfBasicSalesTaxIsApplicableOnProduct() throws InvalidMoneyException {
    Money unitPrice = Money.money(10.00);
    Product nonExemptedProduct = new Product("Non exempted product", unitPrice,true, TaxRateCategory.BASIC);
    Assert.assertTrue(nonExemptedProduct.isBasic());
  }
}
