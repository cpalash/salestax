package twpathashala51.palash.salestax;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static twpathashala51.palash.salestax.Money.*;

public class ReceiptTest {

  @Test
  public void shouldReturnTheSameAmountForExemptedGoods() throws InvalidMoneyException {
    Money unitPrice = money(12.49);
    Product book = new Product("Book A", unitPrice, false, TaxRateCategory.EXEMPTED);
    Receipt receipt = new Receipt(book);

    Money expected = money(12.49);
    assertEquals(expected, receipt.finalAmount());
  }

  @Test
  public void shouldCalculateTheSalesTaxForAGood() throws InvalidMoneyException {
    Money unitPrice = money(14.99);
    Product musicCD = new Product("Music CD", unitPrice,false , TaxRateCategory.BASIC);
    Receipt receipt = new Receipt(musicCD);
    Money expected = money(16.49);

    assertEquals(expected, receipt.finalAmount());
  }

  @Test
  public void shouldCalculateTheTaxOnImportedGood() throws InvalidMoneyException {
    Money unitPrice = money(10.00);
    Product importedChocolate = new Product("Imported Chocolate", unitPrice,true, TaxRateCategory.EXEMPTED);
    Receipt receipt = new Receipt(importedChocolate);

    Money expectedTax = money(10.50);
    assertEquals(expectedTax, receipt.finalAmount());
  }

  @Test
  public void shouldCalculateTheFinalAmountForAListOfProducts() throws InvalidMoneyException {
    Money unitPriceOfBook = money(12.49);
    Money unitPriceOfMusicCD = money(14.99);
    Money unitPriceOfChocolateBar = money(0.85);

    Product book = new Product("Book A", unitPriceOfBook, false, TaxRateCategory.EXEMPTED);
    Product musicCD = new Product("Music CD", unitPriceOfMusicCD, false, TaxRateCategory.BASIC);
    Product chocolateBar = new Product("Chocolate Bar", unitPriceOfChocolateBar, false , TaxRateCategory.EXEMPTED);
    Receipt receipt = new Receipt(book, musicCD, chocolateBar);
    Money expectedAmount = money(29.83);
    assertEquals(expectedAmount, receipt.finalAmount());
  }
  @Test
  public void shouldCalculateSalesTaxForAListOfProduct() throws InvalidMoneyException {
    Money unitPriceOfBook = money(12.49);
    Money unitPriceOfMusicCD = money(14.99);
    Money unitPriceOfChocolateBar = money(0.85);


    Product book = new Product("Book A", unitPriceOfBook, false, TaxRateCategory.EXEMPTED);
    Product musicCD = new Product("Music CD", unitPriceOfMusicCD, false, TaxRateCategory.BASIC);
    Product chocolateBar = new Product("Chocolate Bar", unitPriceOfChocolateBar, false , TaxRateCategory.EXEMPTED);
    Receipt receipt = new Receipt(book, musicCD, chocolateBar);
    Money expectedTax = money(1.50);
    assertEquals(expectedTax, receipt.salesTax());
  }

  @Test
  public void shouldCalculateSalesTaxForAListOfImportedProducts() throws InvalidMoneyException {
    Money unitPriceForImportedChocolate = money(10.00);
    Money unitPriceForImportedPerfume = money(47.50);
    Product importedChocolate = new Product("Imported Chocolate", unitPriceForImportedChocolate, true, TaxRateCategory.EXEMPTED);
    Product importedPerfume = new Product("Imported Perfume", unitPriceForImportedPerfume, true, TaxRateCategory.BASIC);
    Receipt receipt = new Receipt(importedChocolate, importedPerfume);

    Money expectedTax = money(7.65);
    assertEquals(expectedTax, receipt.salesTax());
  }

  @Test
  public void shouldCalculateSalesTaxForAMixOfImportedAndLocalProducts() throws InvalidMoneyException {
    Money unitPriceOfImportedPerfume = money(27.99);
    Money unitPriceOfPerfume = money(18.99);
    Money unitPriceOfHeadachePills = money(9.75);
    Money unitPriceOfImportedChocolate = money(11.25);

    Product importedPerfume = new Product("Imported Perfume", unitPriceOfImportedPerfume, true, TaxRateCategory.BASIC);
    Product perfume = new Product("Perfume", unitPriceOfPerfume, false, TaxRateCategory.BASIC);
    Product headachePills = new Product("Headache Pills", unitPriceOfHeadachePills, false, TaxRateCategory.EXEMPTED);
    Product importedChocolate = new Product("Imported Chocolate", unitPriceOfImportedChocolate, true, TaxRateCategory.EXEMPTED);
    Receipt receipt = new Receipt(importedPerfume, perfume, headachePills, importedChocolate);

    Money expectedTax = money(6.70);
    assertEquals(expectedTax, receipt.salesTax());
  }
}
